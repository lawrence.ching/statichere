# Static Here [![NPM](https://nodei.co/npm/statichere.png)](https://nodei.co/npm/statichere/)

[![pipeline status](https://gitlab.com/lawrence.ching/statichere/badges/master/pipeline.svg)](https://gitlab.com/lawrence.ching/statichere/commits/master)
[![coverage report](https://gitlab.com/lawrence.ching/statichere/badges/master/coverage.svg)](https://gitlab.com/lawrence.ching/statichere/commits/master)
[![NPM Version](http://img.shields.io/npm/v/statichere.svg?style=flat)](https://www.npmjs.org/package/commander)
[![NPM Downloads](https://img.shields.io/npm/dm/statichere.svg?style=flat)](https://www.npmjs.org/package/commander)


This tool helps you to download your server static file
with a simple web UI.

In IT aspect, statichere wraps a UI layer above node-static package.
Why I make it is because I always need to download a set of files like log files.
There are names too long and too hard to type manually, when I'm using node-static.

**Does NOT support Windows at this moment**

## Install
```bash
npm install -g statichere
```

## Usage
Go to the folder where files locate,
```bash
statichere
```

Then access `http://<ip>:8080`.
You will see a web page with all files under the folder.

## Note
Please aware that this tool is REALLY NOT GOOD to use in production environment.