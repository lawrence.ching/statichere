#!/usr/bin/env node


const fs = require('fs');
const os = require('os');
const static = require('node-static');
const fse = require('fs-extra');
const http = require('http');
const path = require('path');
const fsUtils = require('../src/fs-utils');
const getFileSizeSync = fsUtils.getFileSizeSync;

function readTemplateHtml() {
    const binFolderPath = path.dirname(cliFilePath);
    const templateHtmlPath = fs.realpathSync( binFolderPath + '/../template.html');
    return fs.readFileSync(templateHtmlPath, 'UTF-8');
}

require('commander')
    .version(require('../package').version)
    .parse(process.argv);

const cliFilePath = fs.realpathSync(process.argv[1]);
const currentFolder = fs.realpathSync('.');
const tmpFolder = os.tmpdir() + '/' + 'statichere';

if(fs.existsSync(tmpFolder)) {
    fse.removeSync(tmpFolder);
}
fs.mkdirSync(tmpFolder);

const fileFolder = tmpFolder + '/file';
if(!fs.existsSync(fileFolder)) {
    fs.mkdirSync(fileFolder);
}

const filePaths = fs.readdirSync('.');
const files = [];
for (index in filePaths) {
    const name = filePaths[index];
    const url = '/file/' + name;

    const fileRealPath = currentFolder + '/' + name;
    const size = getFileSizeSync(fileRealPath);

    const symlinkPath = fileFolder + '/' + name;

    fs.symlinkSync(fileRealPath, symlinkPath);

    files.push({
        name: name,
        path: url,
        size: size
    })
}

const html = readTemplateHtml().replace('<!--Replace file list here-->', JSON.stringify(files));

fs.writeFile(tmpFolder + '/index.html', html, (err) => {
    if (err) {
        return console.log(err);
    }
});

var fileServer = new static.Server(tmpFolder);

const port = 8080
http.createServer(function (request, response) {
    request.addListener('end', function () {
        fileServer.serve(request, response);
    }).resume();
}).listen(port);

console.log('Serve static files in "' + currentFolder + '"');
console.log('Open HTTP server in http://localhost:%s', port);
