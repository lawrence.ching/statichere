const fsUtils = require('../src/fs-utils');
const assert = require('chai').assert;

describe('fs-utils', function() {
    describe('getFileSize()', function() {
        it('can return file size', function() {
            const getFileSizeSync = fsUtils.getFileSizeSync;
            assert.isAbove(getFileSizeSync('README.md'), 0)
        });
    });
});
