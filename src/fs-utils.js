const fs = require('fs');

function getFileSizeSync(path) {
    const stats = fs.statSync(path);
    const fileSizeInBytes = stats.size;
    return fileSizeInBytes
}

module.exports = {
    getFileSizeSync
}